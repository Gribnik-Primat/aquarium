<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>hello world</title>
<style>
.loader {
  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 8px solid black;
  border-right: 8px solid red;
  border-bottom: 8px solid black;
  border-left: 8px solid red;
  width: 50px;
  height: 50px;
  -webkit-animation: spin 2s linear infinite;
  animation: spin 2s linear infinite;
}

@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
</style>
</head>
<body>
	<script src="jquery-3.3.1.js"></script>
	<script src="gl-matrix.js"></script>
	<script src="client.js"></script>
	<div>
		<input type="text" id="data" name="data" value="3 1 0 3 1 4 1 2" style="width: 600px;"/>
		<button type="button" onclick="submitForm();" id="send_data">Launch</button>
		Maximum field size is 20x20. Each number represents height of the column
	</div>
	<div hidden class="loader" id = "indicator"></div>
	
		<canvas id="glcanvas">
			  Your browser doesn't appear to support the HTML5 canvas element.
		</canvas>

</body>
</html>