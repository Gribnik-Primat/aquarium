

function submitForm() {
    var arrayString = $('#data')[0].value;
    // transform string to array of int
    var arrayNumber = arrayString.trim().split(' ').filter(function(item) { 
        var number = parseInt(item, 10);
        if(number || number == 0){
        	return true;
        }
    });
    arrayNumber = arrayNumber.map(function(item){ 
    	return parseInt(item,10);
    });
     $('#indicator').show();
    $.ajax({ // sending ajax POST request
        url: 'servlet',
        type: 'POST',
        contentType: "application/json",
        dataType:'json',
        data: JSON.stringify(arrayNumber),
        success: function(data) { // getting our json from servlet
           	$('#indicator').hide();
        	calldraw(data);        
        },
        error: function(error) { // getting error from servlet
           	$('#indicator').hide();
            alert(error.responseText);
        }
    });
}

function calldraw(dataFromServlet) {
    const canvas = document.querySelector('#glcanvas');
    canvas.width = window.innerWidth
    canvas.height = window.innerHeight
    const gl = canvas.getContext('webgl') || canvas.getContext('experimental-webgl');

    if (!gl) {
        alert('Unable to initialize WebGL. Your browser or machine may not support it.');
        return;
    }

    // Vertex shader program

    const vsSource = `
	    attribute vec4 aVertexPosition;
	    attribute vec4 aVertexColor;
	    uniform mat4 uModelViewMatrix;
	    uniform mat4 uProjectionMatrix;
	    varying lowp vec4 vColor;
	    void main(void) {
	      gl_Position = uProjectionMatrix * uModelViewMatrix * aVertexPosition;
	      vColor = aVertexColor;
	    }
	  `;

    // Fragment shader program

    const fsSource = `
	    varying lowp vec4 vColor;
	    void main(void) {
	      gl_FragColor = vColor;
	    }
	  `;

    // Initialize a shader program;
    const shaderProgram = initShaderProgram(gl, vsSource, fsSource);

    const programInfo = {
        program: shaderProgram,
        attribLocations: {
            vertexPosition: gl.getAttribLocation(shaderProgram, 'aVertexPosition'),
            vertexColor: gl.getAttribLocation(shaderProgram, 'aVertexColor'),
        },
        uniformLocations: {
            projectionMatrix: gl.getUniformLocation(shaderProgram, 'uProjectionMatrix'),
            modelViewMatrix: gl.getUniformLocation(shaderProgram, 'uModelViewMatrix'),
        },
    };

    // objects we'll be drawing.
    const buffers = initBuffers(gl);

    var then = 0;
    var Rotation = 0.0;
    // Draw the scene
    function render(now) {
    	
        now *= 0.01;
        const deltaTime = now - then;
        then = now;
        Rotation += deltaTime;
        drawScene(gl, programInfo, buffers, deltaTime,Rotation,dataFromServlet);

        requestAnimationFrame(render);

    }
    requestAnimationFrame(render);
}


function initBuffers(gl) {

    // Create a buffer for the cube's vertex positions.

    const positionBuffer = gl.createBuffer();

    gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);

    const positions = [
        // Front face
        -1.0, -1.0, 1.0,
        1.0, -1.0, 1.0,
        1.0, 1.0, 1.0,
        -1.0, 1.0, 1.0,

        // Back face
        -1.0, -1.0, -1.0,
        -1.0, 1.0, -1.0,
        1.0, 1.0, -1.0,
        1.0, -1.0, -1.0,

        // Top face
        -1.0, 1.0, -1.0,
        -1.0, 1.0, 1.0,
        1.0, 1.0, 1.0,
        1.0, 1.0, -1.0,

        // Bottom face
        -1.0, -1.0, -1.0,
        1.0, -1.0, -1.0,
        1.0, -1.0, 1.0,
        -1.0, -1.0, 1.0,

        // Right face
        1.0, -1.0, -1.0,
        1.0, 1.0, -1.0,
        1.0, 1.0, 1.0,
        1.0, -1.0, 1.0,

        // Left face
        -1.0, -1.0, -1.0,
        -1.0, -1.0, 1.0,
        -1.0, 1.0, 1.0,
        -1.0, 1.0, -1.0,
    ];

    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(positions), gl.STATIC_DRAW);

    // set up the colors for the faces.

    const waterColors = [
        [0.0, 0.0, 0.8, 1.0],
        [0.0, 0.0, 0.8, 1.0],
        [0.0, 0.0, 0.5, 1.0],
        [0.0, 0.0, 0.5, 1.0],
        [0.0, 0.0, 0.5, 1.0],
        [0.0, 0.0, 0.5, 1.0],
    ];
    const soilColors = [
        [0.0, 0.8, 0.0, 1.0],
        [0.0, 0.8, 0.0, 1.0],
        [0.0, 0.5, 0.0, 1.0],
        [0.0, 0.5, 0.0, 1.0],
        [0.0, 0.5, 0.0, 1.0],
        [0.0, 0.5, 0.0, 1.0],
    ];



    var tmpSoil = [];
    for (var j = 0; j < soilColors.length; ++j) { // for every vertex
        const c = soilColors[j];
        tmpSoil = tmpSoil.concat(c, c, c, c);
    }

    const colorBufferSoil = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, colorBufferSoil);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(tmpSoil), gl.STATIC_DRAW);

    var tmpWater = [];

    for (var j = 0; j < waterColors.length; ++j) { // for every vertex
        const c = waterColors[j];
        tmpWater = tmpWater.concat(c, c, c, c);
    }

    const colorBufferWater = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, colorBufferWater);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(tmpWater), gl.STATIC_DRAW);

    // Build the element array buffer;

    const indexBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBuffer);

    const indices = [
        0, 1, 2, 0, 2, 3, // front
        4, 5, 6, 4, 6, 7, // back
        8, 9, 10, 8, 10, 11, // top
        12, 13, 14, 12, 14, 15, // bottom
        16, 17, 18, 16, 18, 19, // right
        20, 21, 22, 20, 22, 23, // left
    ];

    // Now send the element array to GL

    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER,
        new Uint16Array(indices), gl.STATIC_DRAW);

    return {
        position: positionBuffer,
        colorSoil: colorBufferSoil,
        colorWater: colorBufferWater,
        indices: indexBuffer,
    };
}

//
// Draw the scene.
//
function drawScene(gl, programInfo, buffers, deltaTime, Rotation ,dataFromServlet) {
    gl.clearColor(0.0, 0.0, 0.0, 1.0);
    gl.clearDepth(1.0);
    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);


    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    // Create a perspective matrix

    const fieldOfView = 45 * Math.PI / 180;
    const aspect = gl.canvas.clientWidth / gl.canvas.clientHeight;
    const zNear = 0.1;
    const zFar = 100.0;
    const projectionMatrix = glMatrix.mat4.create();

    var max = 0;
    for(var i = 0; i < dataFromServlet.length; i++){
    	if(dataFromServlet[i].length > max)
    		max = dataFromServlet[i].length;
    }
    const centerX = dataFromServlet.length;
    const centerY = max;
    const cameraZ = centerX > centerY ? centerX : centerY;
    
    glMatrix.mat4.perspective(projectionMatrix, // making camera
        fieldOfView,
        aspect,
        zNear,
        zFar);
    glMatrix.mat4.translate(projectionMatrix,
        projectionMatrix,
        [-0.0, 0.0, -(cameraZ)*3]);
    glMatrix.mat4.rotate(projectionMatrix, // rotating camera
        projectionMatrix,
        Rotation * Math.PI / 180,
        [0, 1, 0]);

    const modelViewMatrix = glMatrix.mat4.create(); // matrix for our cubes
    
  
    for (var i = 0; i < dataFromServlet.length; i++) {
        for (var j = 0; j < dataFromServlet[i].length; j++) {

            // vertex buffer
            {
                const numComponents = 3;
                const type = gl.FLOAT;
                const normalize = false;
                const stride = 0;
                const offset = 0;
                gl.bindBuffer(gl.ARRAY_BUFFER, buffers.position);
                gl.vertexAttribPointer(
                    programInfo.attribLocations.vertexPosition,
                    numComponents,
                    type,
                    normalize,
                    stride,
                    offset);
                gl.enableVertexAttribArray(
                    programInfo.attribLocations.vertexPosition);
            }

            // color buffer
            {
                const numComponents = 4;
                const type = gl.FLOAT;
                const normalize = false;
                const stride = 0;
                const offset = 0;
                if (dataFromServlet[i][j] == 's') // for soil
                    gl.bindBuffer(gl.ARRAY_BUFFER, buffers.colorSoil);
                if (dataFromServlet[i][j] == 'l') // for water
                    gl.bindBuffer(gl.ARRAY_BUFFER, buffers.colorWater);
                gl.vertexAttribPointer(
                    programInfo.attribLocations.vertexColor,
                    numComponents,
                    type,
                    normalize,
                    stride,
                    offset);
                gl.enableVertexAttribArray(
                    programInfo.attribLocations.vertexColor);
            }

            // index buffer
            gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, buffers.indices);

            gl.useProgram(programInfo.program);


            var cubePos = glMatrix.mat4.create();
            // making translation for a cube
            glMatrix.mat4.translate(modelViewMatrix, 
                cubePos,
                [(i * 2.1) - centerX, (j * 2.1) - centerY, 0.0]);


            gl.uniformMatrix4fv(
                programInfo.uniformLocations.projectionMatrix,
                false,
                projectionMatrix); // setting projection

            gl.uniformMatrix4fv(
                programInfo.uniformLocations.modelViewMatrix,
                false,
                modelViewMatrix); // setting single cube
            const vertexCount = 36;
            const type = gl.UNSIGNED_SHORT;
            const offset = 0;
            gl.drawElements(gl.TRIANGLES, vertexCount, type, offset); // start
            // drawing
        }

    }
}

//
// Initialize a shader program
function initShaderProgram(gl, vsSource, fsSource) {
    const vertexShader = loadShader(gl, gl.VERTEX_SHADER, vsSource);
    const fragmentShader = loadShader(gl, gl.FRAGMENT_SHADER, fsSource);

    // Create the shader program

    const shaderProgram = gl.createProgram();
    gl.attachShader(shaderProgram, vertexShader);
    gl.attachShader(shaderProgram, fragmentShader);
    gl.linkProgram(shaderProgram);

    if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
        alert('Unable to initialize the shader program: ' + gl.getProgramInfoLog(shaderProgram));
        return null;
    }

    return shaderProgram;
}

//
// creates a shader of the given type, uploads the source and
// compiles it.
//
function loadShader(gl, type, source) {
    const shader = gl.createShader(type);

    // trying to Send and compile the source to the shader object

    gl.shaderSource(shader, source);
    gl.compileShader(shader);


    if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
        alert('An error occurred compiling the shaders: ' + gl.getShaderInfoLog(shader));
        gl.deleteShader(shader);
        return null;
    }

    return shader;

}