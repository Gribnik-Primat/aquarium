
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.URLDecoder;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

@WebServlet("/servlet")

public class servlet extends HttpServlet {
	class MyException extends Exception { // class for exceptions
		int id;

		public MyException(int x) {
			id = x;
		}

		public int getid() {
			return id;
		}

		public String toString() {
			if (id == 0)
				return "400 - Bad request\n";
			if (id == 1)
				return "413 - Payload Too Large\n";
			if (id == 2)
				return "422 - Unprocessable Entity\n";
			return "";
		}
	}

	private static final long serialVersionUID = 1L;

	public servlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out = response.getWriter();
		try {
			char[][] array = calculate(request, response); // trying to solve the problem
			if (array != null) {
				Gson gson = new Gson();
				response.setStatus(200);
				response.setContentType("text");
				response.setCharacterEncoding("UTF-8");
				out.write(gson.toJson(array)); // success
				out.flush();
			}
		} catch (MyException e) {
			switch (e.getid()) {
			case 0:
				response.setStatus(400); // Bad request
				break;
			case 1:
				response.setStatus(413); // Too large payload
				break;
			case 2:
				response.setStatus(422); // Unprocessable Entity
				break;
			}
			response.setContentType("text");
			out.write(e.toString());
		}
	}

	private char[][] calculate(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, MyException {
		InputStream is = request.getInputStream();

		// reading json from request
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		byte[] buf = new byte[32];
		int r = 0;
		while (r >= 0) {
			r = is.read(buf);
			if (r >= 0)
				os.write(buf, 0, r);
		}
		String s = new String(os.toByteArray(), "UTF-8");
		String decoded = URLDecoder.decode(s, "UTF-8");
		System.err.println(">>>>>>>>>>>>> DECODED: " + decoded);

		if (decoded != null) {
			Gson gson = new Gson();
			int[] array;

			try {
				array = gson.fromJson(decoded, int[].class);
			} catch (Exception e) {
				throw new MyException(0);
			}

			if (array.length > 20)
				throw new MyException(1); // Payload Too Large

			int max = 0;// this max needed for building our matrix
			for (int i = 0; i < array.length; i++) {
				if (array[i] > max) {
					max = array[i];
				}
				if (array[i] > 20 || array[i] < 0) {
					throw new MyException(2); //
				}
			}

			int[][] field = new int[array.length][max];
			for (int i = 0; i < array.length; i++) {
				for (int j = 0; j < max; j++) {
					if (j < array[i])
						field[i][j] = 2; // 2 for solid
					else
						field[i][j] = 1; // 1 for water
				}
			}

			for (int i = 0; i < array.length; i++) {
				if (i == 0 || i == array.length - 1) {
					for (int j = 0; j < max; j++) {// deleting water in begin and end
						if (j >= array[i]) {
							field[i][j] = 0; // 0 for nothing
						}
					}
				}
				if (field[i][0] == 1) { // deleting water in column there is no soil
					for (int j = 0; j < max; j++) {
						field[i][j] = 0;
					}
				}
			}

			for (int j = 0; j < max; j++) { // deleting other needed blocks
				for (int i = 0; i < array.length; i++) {
					if (i < array.length - 1) {
						if (field[i + 1][j] == 0)
							for (int k = i;; k--) {
								if (k == 0 || field[k][j] == 2)
									break;
								else
									field[k][j] = 0;
							}
					}
					if (i > 0) {
						if (field[i - 1][j] == 0) {
							for (int k = i;; k++) {
								if (k == array.length - 1 || field[k][j] == 2)
									break;
								else
									field[k][j] = 0;
							}
						}
					}
				}
			}
			
			char[][] field_response = new char[array.length][]; //forming back response according to the task
			for(int i = 0;i<array.length;i++) {
				int blocks_in_column = 0;
				for(int k = 0;k<max;k++) {
					if(field[i][k] != 0){ // trimming empty cells
						blocks_in_column++;
					}
				}
				field_response[i] = new char[blocks_in_column];
				for(int j = 0; j < field_response[i].length; j++) { //filling response with chars
					if(field[i][j] == 2)
						field_response[i][j] = 's';
					if(field[i][j] == 1)
						field_response[i][j] = 'l';
				}
			}
			
			return field_response;
		}
		return null;
	}
}
